
'''

The partially defined functions and classes of this module 
will be called by a marker script. 

You should complete the functions and classes according to their specified interfaces.
 

'''

import search

import sokoban



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


def my_team():
    '''
    Return the list of the team members of this assignment submission as a list
    of triplet of the form (student_number, first_name, last_name)
    
    '''
#    return [ (1234567, 'Ada', 'Lovelace'), (1234568, 'Grace', 'Hopper'), (1234569, 'Eva', 'Tardos') ]
    return [ (9163034, 'Cameron', 'Happer') ];

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


def taboo_cells(warehouse):
    '''  
    Identify the taboo cells of a warehouse. A cell is called 'taboo' 
    if whenever a box get pushed on such a cell then the puzzle becomes unsolvable.  
    When determining the taboo cells, you must ignore all the existing boxes, 
    simply consider the walls and the target  cells.  
    Use only the following two rules to determine the taboo cells;
     Rule 1: if a cell is a corner and not a target, then it is a taboo cell.
     Rule 2: all the cells between two corners along a wall are taboo if none of 
             these cells is a target.
    
    @param warehouse: a Warehouse object

    @return
       A string representing the puzzle with only the wall cells marked with 
       an '#' and the taboo cells marked with an 'X'.  
       The returned string should NOT have marks for the worker, the targets,
       and the boxes.  
    '''
    ##         "INSERT YOUR CODE HERE" 
    
    taboo_targets = warehouse.targets;
    taboo = warehouse.walls;
    #taboo = taboo + taboo_targets;
    taboo_cells = [];
    max_x = max(warehouse.walls, key = lambda item:item[0])[0];
    max_y = max(warehouse.walls, key = lambda item:item[1])[1];
               
    ans = '''''';
    min_x, min_y = min(taboo);
    #X = 0;                  
    #for loop finds corners that arent targets
    low_x_coords = [];
    for wall in warehouse.walls:
        x,y = wall;
        i = 1;
        if y == i:
            i = i + 1;
            low_x_coords = low_x_coords + [wall];
    
    print(low_x_coords);
    
    for x,y in taboo:
        x = 0;
        if x == 0 and (y == min_y or y == max_y): 
            ans += '''
#######''';
        elif x == 0: 
            ans += '''
#''';
        counter = 0;
        X = x + 1;
        while X <= max_x:
            if (X, y) not in taboo_targets:
                counter = counter + 1;
            if (X - 1, y) in taboo and ( X, y - 1) in taboo and (X, y) not in taboo and (X,y) not in taboo_targets:
                taboo_cells = taboo_cells + [(X, y)];
                ans += 'X';
            elif (X - 1, y) in taboo and ( X, y + 1) in taboo and (X, y) not in taboo and (X,y) not in taboo_targets:
                taboo_cells = taboo_cells + [(X, y)];
                ans += 'X';
            elif (X + 1, y) in taboo and ( X, y - 1) in taboo and (X, y) not in taboo and (X,y) not in taboo_targets:
                taboo_cells = taboo_cells + [(X, y)];
                ans += 'X';
            elif (X + 1, y) in taboo and ( X, y + 1) in taboo and (X, y) not in taboo and (X,y) not in taboo_targets:
                taboo_cells = taboo_cells + [(X, y)];
                ans += 'X';
            elif x == 0 and X != max_x and y != min_y and y != max_y:
                ans += ' ';
            X += 1;
            
        if x == 0 and y != min_y and y != max_y: 
            ans += '#';  

    warehouse.taboo = taboo_cells;
    print(taboo_cells);
    #print(ans);
    return ans;
   
    #raise NotImplementedError()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


class SokobanPuzzle(search.Problem):
    '''
    Class to represent a Sokoban puzzle.
    Your implementation should be compatible with the
    search functions of the provided module 'search.py'.
    
    	Use the sliding puzzle and the pancake puzzle for inspiration!
    
    '''
    ##         "INSERT YOUR CODE HERE"
    
    def __init__(self, warehouse):
        self.targets = warehouse.targets;
        self.worker = warehouse.worker;
        self.boxes = warehouse.boxes;
        
        #raise NotImplementedError()

    def actions(self, state):
        """
        Return the list of actions that can be executed in the given state 
        if these actions do not push a box in a taboo cell.
        The actions must belong to the list ['Left', 'Down', 'Right', 'Up']        
        """
        
        #return ;
        
        raise NotImplementedError()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def check_action_seq(warehouse, action_seq):
    '''
    
    Determine if the sequence of actions listed in 'action_seq' is legal or not.
    
    Important notes:
      - a legal sequence of actions does not necessarily solve the puzzle.
      - an action is legal even if it pushes a box onto a taboo cell.
        
    @param warehouse: a valid Warehouse object

    @param action_seq: a sequence of legal actions.
           For example, ['Left', 'Down', Down','Right', 'Up', 'Down']
           
    @return
        The string 'Failure', if one of the action was not successul.
           For example, if the agent tries to push two boxes at the same time,
                        or push one box into a wall.
        Otherwise, if all actions were successful, return                 
               A string representing the state of the puzzle after applying
               the sequence of actions.  This must be the same string as the
               string returned by the method  Warehouse.__str__()
    '''
    
    ##         "INSERT YOUR CODE HERE"
    print(action_seq);
    #print(warehouse.__str__());
    wx,wy = warehouse.worker;
    ##isnt checking for taboo cells unsure yet. and doesnt check boxes
    for action in action_seq:
        #for box in warehouse.boxes:
            #a, b = box
        if action == 'Right':
            if (wx + 1, wy) == warehouse.walls: # or (a + 1, b) == (warehouse.walls or warehouse.boxes):
                return 'Failure';
            else:
                wx = wx + 1;
        if action == 'Left':
            if (wx - 1, wy) == warehouse.walls: # or (a - 1, b) == (warehouse.walls or warehouse.boxes):
                return 'Failure';
            else:
                wx = wx - 1;
        if action == 'Down':
            if (wx, wy + 1) == warehouse.walls: # or (a, b + 1) == (warehouse.walls or warehouse.boxes):
                return 'Failure';
            else:
                wy = wy + 1;
        if action == 'Up':
            if (wx, wy - 1) == warehouse.walls: # or (a, b - 1) == (warehouse.walls or warehouse.boxes):
                return 'Failure';
            else:
                wy = wy - 1;
                        
    warehouse.worker = (wx, wy);
    #print(warehouse.__str__());
    
          
    print(warehouse.walls);
          
    return warehouse.__str__();
    #raise NotImplementedError()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def solve_sokoban_elem(warehouse):
    '''    
    This function should solve using elementary actions 
    the puzzle defined in a file.
    
    @param warehouse: a valid Warehouse object

    @return
        A list of strings.
        If puzzle cannot be solved return ['Impossible']
        If a solution was found, return a list of elementary actions that solves
            the given puzzle coded with 'Left', 'Right', 'Up', 'Down'
            For example, ['Left', 'Down', Down','Right', 'Up', 'Down']
            If the puzzle is already in a goal state, simply return []
    '''
    
    ##         "INSERT YOUR CODE HERE"
    
    raise NotImplementedError()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def can_go_there(warehouse, dst):
    '''    
    Determine whether the worker can walk to the cell dst=(row,col) 
    without pushing any box.
    
    @param warehouse: a valid Warehouse object

    @return
      True if the worker can walk to cell dst=(row,col) without pushing any box
      False otherwise
    '''
    
    ##         "INSERT YOUR CODE HERE"
    
    raise NotImplementedError()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def solve_sokoban_macro(warehouse):
    '''    
    Solve using macro actions the puzzle defined in the warehouse passed as
    a parameter. A sequence of macro actions should be 
    represented by a list M of the form
            [ ((r1,c1), a1), ((r2,c2), a2), ..., ((rn,cn), an) ]
    For example M = [ ((3,4),'Left') , ((5,2),'Up'), ((12,4),'Down') ] 
    means that the worker first goes the box at row 3 and column 4 and pushes it left,
    then goes the box at row 5 and column 2 and pushes it up, and finally
    goes the box at row 12 and column 4 and pushes it down.
    
    @param warehouse: a valid Warehouse object

    @return
        If puzzle cannot be solved return ['Impossible']
        Otherwise return M a sequence of macro actions that solves the puzzle.
        If the puzzle is already in a goal state, simply return []
    '''
    
    ##         "INSERT YOUR CODE HERE"
    
    raise NotImplementedError()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

