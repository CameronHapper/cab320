
'''

The partially defined functions and classes of this module 
will be called by a marker script. 

You should complete the functions and classes according to their specified interfaces.
 

'''

import search
import time
import sokoban


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


def my_team():
    '''
    Return the list of the team members of this assignment submission as a list
    of triplet of the form (student_number, first_name, last_name)
    
    '''
#    return [ (1234567, 'Ada', 'Lovelace'), (1234568, 'Grace', 'Hopper'), (1234569, 'Eva', 'Tardos') ]
    return [ (9163034, 'Cameron', 'Happer'), (9294601, 'Brenton', 'Wheeler') , ( 9430440, 'Ivan', 'De Courtenay')];



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


def taboo_cells(warehouse):
    '''  
    Identify the taboo cells of a warehouse. A cell is called 'taboo' 
    if whenever a box get pushed on such a cell then the puzzle becomes unsolvable.  
    When determining the taboo cells, you must ignore all the existing boxes, 
    simply consider the walls and the target  cells.  
    Use only the following two rules to determine the taboo cells;
     Rule 1: if a cell is a corner and not a target, then it is a taboo cell.
     Rule 2: all the cells between two corners along a wall are taboo if none of 
             these cells is a target.
    
    @param warehouse: a Warehouse object

    @return
       A string representing the puzzle with only the wall cells marked with 
       an '#' and the taboo cells marked with an 'X'.  
       The returned string should NOT have marks for the worker, the targets,
       and the boxes.  
    '''
    
    #always start from the top right corner (0,1).
    x = 0; y = 1;
    
    #get the max y and max x values of the walls.
    max_x = max(warehouse.walls, key = lambda item:item[0])[0];
    max_y = max(warehouse.walls, key = lambda item:item[1])[1];           
    area = [(0,1)];
    
    #while 0,1 isnt max-x,max-y find out if its within the walls.        
    while x != max_x + 1 and y != max_y + 1:
        
        #assign new goal
        warehouse.goal = (x, y);
        #create the puzzle with initial i.e the worker and the goal.
        puzzle = SokobanPuzzle(warehouse);
        #use breadth_first_tree_search to find the goal as it gaurantees to find a goal before all walls.
        sol_ts = search.breadth_first_tree_search(puzzle);
        #increment x until it reaches max x then change x to 0 and increment y                                         
        x = x + 1;
        if x == max_x + 1:
            x = 0;
            y = y + 1;
        #add all the goals between 0,1 and max-x,max-y to an tuple.     
        area = area + [ (x,y) ];

    #steps is a tuple of coords that are inside the walls.                                
    steps = puzzle.contains;
    
    #start creating the returned string     
    str = '''''';
    #go through every coord between 0,1 and max-x,max-y
    for coord in area:
        x,y = coord;
        #if the coord is a wall then add a '#' to the string.
        if coord in warehouse.walls:
            str = str + '#';
        #if the coord is a target change it to a ' ' and add to string.
        elif coord in warehouse.targets:
            str = str + ' ';
        #if the coord is within the area of the walls check if its in a corner, if it is add an 'X' to the string.
        elif coord in steps:
            if (((x - 1, y) in warehouse.walls and (x, y - 1) in warehouse.walls)
            or ((x - 1, y) in warehouse.walls and (x, y + 1) in warehouse.walls)
            or ((x + 1, y) in warehouse.walls and (x, y - 1) in warehouse.walls)
            or ((x + 1, y) in warehouse.walls and (x, y + 1) in warehouse.walls)):
                str = str + 'X';
            #if the coord is next to a wall check along the wall to see if there is a target.
            elif (x + 1,y) in warehouse.walls or (x - 1, y) in warehouse.walls:
                y = 1;
                while y <= max_y:
                    if (x, y) in warehouse.targets:
                        y = max_y;
                    else:
                        y = y + 1;
                #if no targets are found then place an 'X' along each spot in the wall.
                if y == max_y:
                    str = str + 'X';
                else:
                    str = str + ' ';
            elif (x, y + 1) in warehouse.walls or (x, y - 1) in warehouse.walls:
                x = 0;
                while x <= max_x:
                    if (x, y) in warehouse.targets:
                        x = max_x;
                    else:
                        x = x + 1;
                if x == max_x:
                    str = str + 'X'; 
                else:
                    #if it isn't add a ' ' to the string.
                    str = str + ' ';
            else:
                str = str + ' ';
        else:
            str = str + ' ';
        #if max_x is reached then go to a new line.
        if x == max_x:
            str = str + '''
''';
          
    return str;

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


class SokobanPuzzle(search.Problem):
    '''
    Class to represent a Sokoban puzzle.
    Your implementation should be compatible with the
    search functions of the provided module 'search.py'.
    
    	Use the sliding puzzle and the pancake puzzle for inspiration!
    
    '''

    def __init__(self, warehouse):
        #initialise important values
        self.targets = warehouse.targets;
        self.initial = warehouse.worker;
        self.boxes = warehouse.boxes;
        self.walls = warehouse.walls;
        self.goal = warehouse.goal;
        self.max_y = max(warehouse.walls, key = lambda item:item[1])[1];
        self.max_x = max(warehouse.walls, key = lambda item:item[0])[0];
        self.contains = [];
        self.wallcount = [];
        self.complete = True;

        
        #raise NotImplementedError()

    def actions(self, state):
        """
        Return the list of actions that can be executed in the given state 
        if these actions do not push a box in a taboo cell.
        The actions must belong to the list ['Left', 'Down', 'Right', 'Up']        
        """
        #get x,y coord of worker.
        x,y = state
        L = []  
        #check the bounds of the walls to see which moves are avaialable.
        if y > 1:
            L.append('Up')
        if y < self.max_y:
            L.append('Down')
        if x > 0:
            L.append('Left')
        if x < self.max_x:
            L.append('Right')
        #return a list of the possible workers actions
        return L

    def result(self, state, action):
        """
        Return the state that results from executing the given
        action in the given state. The action must be one of
        self.actions(state).
        """
        #get x,y coord of the current state.
        x,y = state;
        
        assert action in self.actions(state) 
        
        #check if the action is moving into a wall.
        if action == 'Up' and (x, y - 1) not in self.walls:
            #if the action isn't moving into the wall then move the worker
            y = y - 1;
            #if the square hasn't been stepped on then add to the list self.contains.
            if (x,y) not in self.contains:
                #self.contains holds the coords of every coord within the walls of the map.
                self.contains = self.contains + [(x,y)];
        if action == 'Down' and (x, y + 1) not in self.walls:
            y = y + 1;
            if (x,y) not in self.contains:
                self.contains = self.contains + [(x,y)];
        if action == 'Left' and (x - 1, y) not in self.walls:
            x = x - 1;
            if (x,y) not in self.contains:
                self.contains = self.contains + [(x,y)];
        if action == 'Right' and (x + 1, y ) not in self.walls:
            x = x + 1;
            if (x,y) not in self.contains:
                self.contains = self.contains + [(x,y)];
        
        #Check each square to see if its next to wall then add to a list called self.wallcount.
        if (x + 1, y) in self.walls and (x + 1, y) not in self.wallcount:
            #add the coord of the wall to wall count if it is not already in the list.
            self.wallcount = self.wallcount + [(x+1,y)];
            #self.wallcount holds the coords of every wall touched by the worker.
        if (x - 1, y) in self.walls and (x - 1, y) not in self.wallcount:
            self.wallcount = self.wallcount + [(x-1,y)];
        if (x, y + 1) in self.walls and (x, y + 1) not in self.wallcount:
            self.wallcount = self.wallcount + [(x,y+1)];
        if (x, y - 1) in self.walls and (x, y - 1) not in self.wallcount:
            self.wallcount = self.wallcount + [(x,y-1)]; 
                                               
                                          
        #addittional checks to check if there is a wall in a corner which is not reachable by the worker.                                       
        if (x - 1, y - 1) in self.walls and (x - 1, y - 1) not in self.wallcount:
            self.wallcount = self.wallcount + [(x - 1,y - 1)];
        if (x + 1, y - 1) in self.walls and (x + 1, y - 1) not in self.wallcount:
            self.wallcount = self.wallcount + [(x + 1,y - 1)];
        if (x - 1, y + 1) in self.walls and (x - 1, y + 1) not in self.wallcount:
            self.wallcount = self.wallcount + [(x - 1,y + 1)];
        if (x + 1, y + 1) in self.walls and (x + 1, y + 1) not in self.wallcount:
            self.wallcount = self.wallcount + [(x + 1,y + 1)];
        
        #if every wall is touched before a goal then the goal is unreachable i.e out of the bounds of the walls.                                       
        if len(self.wallcount) == len(self.walls):
            self.complete = False;
            return self.goal;
        
        #move onto next state with new x and y coords for worker
        next_state = (x, y);
        return tuple(next_state);  
        
    
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def check_action_seq(warehouse, action_seq):
    '''
    
    Determine if the sequence of actions listed in 'action_seq' is legal or not.
    
    Important notes:
      - a legal sequence of actions does not necessarily solve the puzzle.
      - an action is legal even if it pushes a box onto a taboo cell.
        
    @param warehouse: a valid Warehouse object

    @param action_seq: a sequence of legal actions.
           For example, ['Left', 'Down', Down','Right', 'Up', 'Down']
           
    @return
        The string 'Failure', if one of the action was not successul.
           For example, if the agent tries to push two boxes at the same time,
                        or push one box into a wall.
        Otherwise, if all actions were successful, return                 
               A string representing the state of the puzzle after applying
               the sequence of actions.  This must be the same string as the
               string returned by the method  Warehouse.__str__()
    '''
    
    
    x, y = warehouse.worker;
    #for every action in the sequence check if it hits a wall.
    for action in action_seq:
        #if the action doesn't hit a wall move the worker to their new (x,y)
        if action == 'Up' and (x, y - 1) not in warehouse.walls:
            #if the action doesn't hit a wall check if it hits a box and if there is another box behind it.
            if (x, y - 1) in warehouse.boxes and (x, y - 2) in warehouse.boxes:
                #if the worker pushes a box into another box return fail.
                return 'Failure';
            else:
                #if worker doesn't push box into another box move the box.
                box_x = x;
                box_y = y - 2;
            y = y - 1;
        if action == 'Down' and (x, y + 1) not in warehouse.walls:
            if (x, y + 1) in warehouse.boxes and (x, y + 2) in warehouse.boxes:
                return 'Failure';
            else:
                box_x = x;
                box_y = y + 2;
            y = y + 1;
        if action == 'Left' and (x - 1, y) not in warehouse.walls:
            if (x - 1, y) in warehouse.boxes and (x - 2, y) in warehouse.boxes:
                return 'Failure';
            else:
                box_x = x - 2;
                box_y = y;
            x = x - 1;
        if action == 'Right' and (x + 1, y ) not in warehouse.walls:
            if (x + 1, y) in warehouse.boxes and (x + 2, y) in warehouse.boxes:
                return 'Failure';
            else:
                box_x = x + 2;
                box_y = y;
            x = x + 1;
        
        #after action is complete if the worker is on the square the box was on then move the box to new coord.
        for box in warehouse.boxes:
            if box == warehouse.worker:
                box = (box_x, box_y);
    #after action sequence done then assign the worker back to his coords for str return.
    warehouse.worker = (x,y);               
    
    return warehouse.__str__();

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def solve_sokoban_elem(warehouse):
    '''    
    This function should solve using elementary actions 
    the puzzle defined in a file.
    
    @param warehouse: a valid Warehouse object

    @return
        A list of strings.
        If puzzle cannot be solved return ['Impossible']
        If a solution was found, return a list of elementary actions that solves
            the given puzzle coded with 'Left', 'Right', 'Up', 'Down'
            For example, ['Left', 'Down', Down','Right', 'Up', 'Down']
            If the puzzle is already in a goal state, simply return []
    '''      
    path = [];   
    for box in warehouse.boxes:
        targetindex = 0;
        bx,by = box;
        warehouse.goal = (bx,by);
        puzzle = SokobanPuzzle(warehouse);

        box_node = search.breadth_first_tree_search(puzzle);
        if box_node.solution()[len(box_node.solution()) - 1] == 'Right':
            warehouse.worker = (bx - 1, by);
        if box_node.solution()[len(box_node.solution()) - 1] == 'Left':
            warehouse.worker = (bx + 1, by);
        if box_node.solution()[len(box_node.solution()) - 1] == 'Up':
            warehouse.worker = (bx, by - 1);
        if box_node.solution()[len(box_node.solution()) - 1] == 'Down':
            warehouse.worker = (bx, by + 1);
        print(box_node.solution());
        print(warehouse.targets);
        for target in warehouse.targets:
            print(warehouse.worker);
            print(target);
            x,y = target; 
            warehouse.goal = (x, y);
            puzzle = SokobanPuzzle(warehouse);
            goal_node = search.breadth_first_tree_search(puzzle);   
            print(goal_node.solution());    
            if goal_node == search.Node(target):
                warehouse.targets.pop(targetindex);
                print(warehouse.targets);
                break;   
            else:
                targetindex = targetindex + 1; 
                if targetindex == len(warehouse.targets) + 1:
                    return ['Impossible'];
                
        #print(goal_node.solution());        
        if goal_node.solution()[len(goal_node.solution()) - 1] == 'Right':
            warehouse.worker = (x - 1, y);
        if goal_node.solution()[len(goal_node.solution()) - 1] == 'Left':
           warehouse.worker = (x + 1, y);
        if goal_node.solution()[len(goal_node.solution()) - 1] == 'Up':
            warehouse.worker = (x, y - 1);
        if goal_node.solution()[len(goal_node.solution()) - 1] == 'Down':
            warehouse.worker = (x, y + 1); 
                                   
        path_count = 0;
        while path_count != len(box_node.solution()) - 1:
            path = path + [box_node.solution()[path_count]];
            path_count = path_count + 1;
            
        path_count = 0;
        while path_count != len(goal_node.solution()) - 1:
            path = path + [goal_node.solution()[path_count]];
            path_count = path_count + 1;
            
                                     
    return path;                                

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def can_go_there(warehouse, dst):
    '''    
    Determine whether the worker can walk to the cell dst=(row,col) 
    without pushing any box.
    
    @param warehouse: a valid Warehouse object

    @return
      True if the worker can walk to cell dst=(row,col) without pushing any box
      False otherwise
    '''
    
    #dst is row, col not x,y so convert row to y and col to x
    y,x = dst;
    warehouse.goal = (x,y);
    
    #assign boxes as walls since we are seeing if a space is available without moving a box
    warehouse.walls = warehouse.walls + warehouse.boxes;
    #warehouse.cango = warehouse.boxes;
    puzzle = SokobanPuzzle(warehouse);    
    sol_ts = search.breadth_first_tree_search(puzzle);
                                             
    #puzzle is solved and returns whether or not the goal is achievable
    if sol_ts == search.Node(warehouse.goal) and puzzle.complete == True:
        return True;
    elif sol_ts == search.Node(warehouse.goal) and puzzle.complete == False:
        return False;

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def solve_sokoban_macro(warehouse):
    '''    
    Solve using macro actions the puzzle defined in the warehouse passed as
    a parameter. A sequence of macro actions should be 
    represented by a list M of the form
            [ ((r1,c1), a1), ((r2,c2), a2), ..., ((rn,cn), an) ]
    For example M = [ ((3,4),'Left') , ((5,2),'Up'), ((12,4),'Down') ] 
    means that the worker first goes the box at row 3 and column 4 and pushes it left,
    then goes the box at row 5 and column 2 and pushes it up, and finally
    goes the box at row 12 and column 4 and pushes it down.
    
    @param warehouse: a valid Warehouse object

    @return
        If puzzle cannot be solved return ['Impossible']
        Otherwise return M a sequence of macro actions that solves the puzzle.
        If the puzzle is already in a goal state, simply return []
    '''
    
    ##         "INSERT YOUR CODE HERE"
    path = [];
    for box in warehouse.boxes:
        targetindex = 0;
        x,y = box;
        warehouse.worker = (x,y);
        for target in warehouse.targets:
            x,y = target; 
            warehouse.goal = (x, y)
            puzzle = SokobanPuzzle(warehouse);
            goal_node = search.breadth_first_tree_search(puzzle);
            if goal_node == search.Node(target):
                warehouse.targets.pop(targetindex);
                break;   
            else:
                targetindex = targetindex + 1; 
                if targetindex == len(warehouse.targets) + 1:
                    return ['Impossible'];    
        x,y = box;        
        for action in goal_node.solution():
            if action == 'Right':
                path = path + [ ((y,x), 'Right')];
                x = x + 1;
            if action == 'Left':
                path = path + [ ((y,x), 'Left')];
                x = x - 1;
            if action == 'Up':
                path = path + [ ((y,x), 'Up')];
                y = y - 1;
            if action == 'Down':
                path = path + [ ((y,x), 'Down')];
                y = y + 1;
         
                                     
    return path; 

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#Helper Methods
